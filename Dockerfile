FROM sutty/monit:latest
MAINTAINER "f <f@sutty.nl>"

RUN apk add --no-cache redis

COPY ./monit.conf /etc/monit.d/redis.conf

RUN sed -re "/^bind /d" -i /etc/redis.conf
RUN sed -re "/^protected-mode /d" -i /etc/redis.conf
RUN sed -re "/^logfile /d" -i /etc/redis.conf
RUN sed -re "s/^# (syslog-enabled).*/\1 yes/" -i /etc/redis.conf
RUN sed -re "s/^(appendonly).*/\1 yes/" -i /etc/redis.conf
RUN echo "pidfile /run/redis/redis.pid" >> /etc/redis.conf
RUN echo "daemonize yes" >> /etc/redis.conf
RUN echo "protected-mode no" >> /etc/redis.conf
RUN echo "maxmemory 50mb" >> /etc/redis.conf
RUN echo "maxmemory-policy volatile-ttl" >> /etc/redis.conf
RUN echo "bind 127.0.0.1 ::1" >> /etc/redis.conf
RUN sed -re "s/^(dir).*/\1 \/var\/lib\/rspamd-redis/" -i /etc/redis.conf

RUN apk add --no-cache rspamd rspamd-client rspamd-proxy rspamd-controller
RUN install -dm 755 /etc/rspamd/local.d
RUN install -dm 750 -o rspamd -g rspamd /var/lib/rspamd
RUN install -dm 750 -o redis -g redis /var/lib/rspamd-redis

# TODO: Deprecate OpenDKIM
RUN echo "enabled = false;" >> /etc/rspamd/local.d/dkim_signing.conf

# Redis
RUN echo "write_servers = \"localhost\";" >> /etc/rspamd/local.d/redis.conf
RUN echo "read_servers = \"localhost\";" >> /etc/rspamd/local.d/redis.conf

# Workers
RUN echo "bind_socket = \"*:11332\";" >> /etc/rspamd/local.d/worker-proxy.inc
RUN echo "bind_socket = \"*:11333\";" >> /etc/rspamd/local.d/worker-normal.inc
RUN echo "bind_socket = \"*:11334\";" >> /etc/rspamd/local.d/worker-controller.inc
# We don't really care about the password...
RUN echo "password = \"`rspamadm pw -p '12345678'`\";" >> /etc/rspamd/local.d/worker-controller.inc

# Options
# Rspamd doesn't seem to write a pid file and upstream not really
# helpful about it: https://github.com/rspamd/rspamd/issues/3096
# RUN echo "pid_file = \"/tmp/rspamd.pid\";" >> /etc/rspamd/local.d/options.inc
RUN echo "local_addrs = \"/etc/rspamd/local.d/maps.d/local_addrs\";" >> /etc/rspamd/local.d/options.inc

# Logging
RUN echo "type = \"syslog\";" >> /etc/rspamd/local.d/logging.inc
RUN echo "facility = \"daemon\";" >> /etc/rspamd/local.d/logging.inc

# Learn spam
RUN echo "servers = \"localhost\";" >> /etc/rspamd/local.d/classifier-bayes.conf
RUN echo "autolearn = [-5,5];" >> /etc/rspamd/local.d/classifier-bayes.conf

COPY ./local_addrs.sh /usr/local/bin/local_addrs
COPY ./rspamd.conf /etc/monit.d/rspamd.conf

EXPOSE 11332
EXPOSE 11333
EXPOSE 11334
VOLUME "/var/lib/rspamd-redis"
VOLUME "/var/lib/rspamd"
