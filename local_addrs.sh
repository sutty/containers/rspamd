#!/bin/sh

local_addrs="/etc/rspamd/local.d/maps.d/local_addrs"

test -f "${local_addrs}" && exit

ip -4 route | cut -d " " -f 1 | grep -v default >> "${local_addrs}"
ip -6 route | cut -d " " -f 1 | grep -v default >> "${local_addrs}"
